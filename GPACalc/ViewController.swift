//
//  ViewController.swift
//  GPACalc
//
//  Created by Yashar Atajan on 2/15/18.
//  Copyright © 2018 Yashar Atajan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tfCourseTitle: UITextField!
    @IBOutlet weak var tfCredits: UITextField!
    @IBOutlet weak var tfAssignmentP: UITextField!
    @IBOutlet weak var tfAssignmentM: UITextField!
    @IBOutlet weak var tfAssignmentW: UITextField!
    @IBOutlet weak var tfMidtermP: UITextField!
    @IBOutlet weak var tfMidtermM: UITextField!
    @IBOutlet weak var tfMidtermW: UITextField!
    @IBOutlet weak var tfFinalP: UITextField!
    @IBOutlet weak var tfFinalM: UITextField!
    @IBOutlet weak var tfFinalW: UITextField!
    @IBOutlet weak var tfCourseNumber: UITextField!
    
    @IBOutlet var tfNumbers: [UITextField]!
    
    @IBOutlet weak var lb1: UILabel!
    @IBOutlet weak var lb2: UILabel!
    @IBOutlet weak var lb3: UILabel!
    @IBOutlet weak var lb4: UILabel!
    @IBOutlet weak var lbG1: UILabel!
    @IBOutlet weak var lbG2: UILabel!
    @IBOutlet weak var lbG3: UILabel!
    @IBOutlet weak var lbG4: UILabel!
    
    @IBOutlet weak var btAdd: UIButton!
    @IBOutlet weak var btRemove: UIButton!
    
    @IBOutlet weak var lbGPA: UILabel!
    
    var weight1: Double = 0
    var weight2: Double = 0
    var weight3: Double = 0
    var weight4: Double = 0
    
    var grade1: Double = 4
    var grade2: Double = 4
    var grade3: Double = 4
    var grade4: Double = 4
    
    var gpa: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btRemove.isEnabled = false
        
        tfCourseTitle.delegate = self
        tfCredits.delegate = self
        tfAssignmentP.delegate = self
        tfAssignmentM.delegate = self
        tfAssignmentW.delegate = self
        tfMidtermP.delegate = self
        tfMidtermM.delegate = self
        tfMidtermW.delegate = self
        tfFinalP.delegate = self
        tfFinalM.delegate = self
        tfFinalW.delegate = self
        tfCourseNumber.delegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Checks that all fields are filled properly before adding the course
    @IBAction func btAdd(_ sender: Any) {
        
        var numberArray = [String]()
        var counter: Int = 0
        
        for i in 0...9 {
            numberArray.append(tfNumbers[i].text!)
        }
        
        for number in numberArray {
            if number.isEmpty {
                
                let alertController = UIAlertController(title: "ERROR", message:
                    "Fill in all the course garades!", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Sir!", style: UIAlertActionStyle.default,handler: nil))
                self.present(alertController, animated: true, completion: nil)
            } else {
                counter = counter + 1
            }
        }
        
        if (counter != 10 || (tfCourseTitle.text?.isEmpty)!) {
            let alertController = UIAlertController(title: "ERROR", message:
                "Fill in course title too!", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Sir!", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        } else {
            errorCheckOne()
        }
    }
    
    @IBAction func btRemove(_ sender: Any) {
        
        let courseNum = tfCourseNumber.text
        
        switch courseNum {
        case "1"?:
            lb1.text = ""
            lbG1.text = ""
            weight1 = 0
            grade1 = 4
        case "2"?:
            lb2.text = ""
            lbG2.text = ""
            weight2 = 0
            grade2 = 4
        case "3"?:
            lb3.text = ""
            lbG3.text = ""
            weight3 = 0
            grade3 = 4
        case "4"?:
            lb4.text = ""
            lbG4.text = ""
            weight4 = 0
            grade4 = 4
        default:
            let alertController = UIAlertController(title: "ERROR", message:
                "Choose the course number from 1 to 4 to delete!", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Sir!", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        
        calcGPA()
    }
    
    func errorCheckOne() {
        
        var indicator: Bool = false
        
        for i in 1 ... 100 {
            
            let ss1 = "1: \(tfCourseTitle.text!) | \(i)"
            let ss2 = "2: \(tfCourseTitle.text!) | \(i)"
            let ss3 = "3: \(tfCourseTitle.text!) | \(i)"
            let ss4 = "4: \(tfCourseTitle.text!) | \(i)"
            
            if (ss1 == lb1.text! || ss2 == lb2.text! || ss3 == lb3.text! || ss4 == lb4.text!) {
                let alertController = UIAlertController(title: "ERROR", message:
                    "Duplicated Course Exists", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "Sir!", style: UIAlertActionStyle.default,handler: nil))
                self.present(alertController, animated: true, completion: nil)
                
                indicator = true
            }
        }
        
        if (Int(tfCredits.text!)! <= 0) {
            let alertController = UIAlertController(title: "ERROR", message:
                "Credit can't be 0", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Sir!", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        } else if(!indicator) {
            addCourse()
        }
    }
    
    func addCourse() {
        
        if (self.lb1.text == "" || self.lb1.text == nil) {
            gradeCalc(line: 1)
        } else if (self.lb2.text == "" || self.lb2.text == nil) {
            gradeCalc(line: 2)
        } else if (self.lb3.text == "" || self.lb3.text == nil) {
            gradeCalc(line: 3)
        } else if (self.lb4.text == "" || self.lb4.text == nil) {
            gradeCalc(line: 4)
        } else {
            let alertController = UIAlertController(title: "ERROR", message:
                "Can't have more than 4 classes!", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func gradeCalc(line: Int){
        
        var negatived: Bool = false
        
        for i in 0 ... 9 {
            
            if (Int(tfNumbers[i].text!)! < 0){
                negatived = true
            }
            
        }

        
        if ((Double(tfAssignmentP.text!)! / Double(tfAssignmentM.text!)!) > 1) {
            
            let alertController = UIAlertController(title: "ERROR", message:
                "Assignment point cant be more than max!", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
            
        } else if ((Double(tfMidtermP.text!)! / Double(tfMidtermM.text!)!) > 1) {
            
            let alertController = UIAlertController(title: "ERROR", message:
                "Midterm point cant be more than max!", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
            
        } else if ((Double(tfFinalP.text!)! / Double(tfFinalM.text!)!) > 1) {
            
            let alertController = UIAlertController(title: "ERROR", message:
                "Final point cant be more than max!", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
            
        } else if ((Double(tfAssignmentW.text!)! + Double(tfMidtermW.text!)! + Double(tfFinalW.text!)!) > 100) {
            
            
            let alertController = UIAlertController(title: "ERROR", message:
                "Total % combined should equal 100!", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
            
        } else if (negatived == true) {
            let alertController = UIAlertController(title: "ERROR", message:
                "Can't have negative values!", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Got it", style: UIAlertActionStyle.default,handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        
        else {
            
            let AssignmentT: Double = (Double(tfAssignmentP.text!)! / Double(tfAssignmentM.text!)!) * ((Double(tfAssignmentW.text!)!) / 100)
            let MidtermT: Double = (Double(tfMidtermP.text!)! / Double(tfMidtermM.text!)!) * ((Double(tfMidtermW.text!)!) / 100)
            let FinalT: Double = (Double(tfFinalP.text!)! / Double(tfFinalM.text!)!) * ((Double(tfFinalW.text!)!) / 100)
            
            let Total: Int = Int((AssignmentT + MidtermT + FinalT) * 100)
            
            let location: Int = line
            
            gradeCalc2(Total: Total, line: location)
        }
}
    
    func gradeCalc2(Total: Int, line: Int) {
        
        if (Total >= 90){
            printGrade(grade: "A", gradeI: 4, line: line)
        } else if (Total >= 80) {
            printGrade(grade: "B", gradeI: 3, line: line)
        } else if (Total >= 70) {
            printGrade(grade: "C", gradeI: 2, line: line)
        } else if (Total >= 60) {
            printGrade(grade: "D", gradeI: 1, line: line)
        } else {
            printGrade(grade: "F", gradeI: 0, line: line)
        }
    }
    
    func printGrade(grade: String, gradeI: Double, line: Int){

        switch line {
        case 1:
            lb1.text = "1: \(tfCourseTitle.text!) | \(tfCredits.text!)"
            lbG1.text = grade
            grade1 = gradeI
            weight1 = Double(tfCredits.text!)!
        case 2:
            lb2.text = "2: \(tfCourseTitle.text!) | \(tfCredits.text!)"
            lbG2.text = grade
            grade2 = gradeI
            weight2 = Double(tfCredits.text!)!
        case 3:
            lb3.text = "3: \(tfCourseTitle.text!) | \(tfCredits.text!)"
            lbG3.text = grade
            grade3 = gradeI
            weight3 = Double(tfCredits.text!)!
        case 4:
            lb4.text = "4: \(tfCourseTitle.text!) | \(tfCredits.text!)"
            lbG4.text = grade
            grade4 = gradeI
            weight4 = Double(tfCredits.text!)!
        default:
            let alertController = UIAlertController(title: "ERROR", message:
                "This should not happen!", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))

            self.present(alertController, animated: true, completion: nil)
        }
        
        calcGPA()
        btRemove.isEnabled = true
    }
    
    func calcGPA() {
        
        var over: Double = 0
        var below: Double = 0
        
        over = (((grade1) * weight1) + ((grade2) * weight2) + ((grade3) * weight3) + ((grade4) * weight4))
        below = (((4) * weight1) + ((4) * weight2) + ((4) * weight3) + ((4) * weight4))
        
        gpa = ((over/below) * 4)
        
        lbGPA.text = String(round(100*gpa)/100)
        
        if (gpa <= 2.0){
            lbGPA.textColor = UIColor.red
        } else if (2 < gpa && gpa <= 3){
            lbGPA.textColor = UIColor.orange
        } else {
            lbGPA.textColor = UIColor.green
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tfCredits.resignFirstResponder()
        tfAssignmentP.resignFirstResponder()
        tfAssignmentM.resignFirstResponder()
        tfAssignmentW.resignFirstResponder()
        tfMidtermP.resignFirstResponder()
        tfMidtermM.resignFirstResponder()
        tfMidtermW.resignFirstResponder()
        tfFinalP.resignFirstResponder()
        tfFinalM.resignFirstResponder()
        tfFinalW.resignFirstResponder()
        tfCourseNumber.resignFirstResponder()
    }
}

extension ViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
