# GPA Calculator
This is a quick gpa calculator app I created for fun. You can put in assignment, midterm, and final grades, as well as their weight/credit to calculate your expected gpa.

The first screen will look something like this (little obnoxious, I know…): 
![SS1](./images/screenshot.png)

You can add a class as follows. Just click add course after filling out the form.
![SS2](./images/screenshot.png)

You can add more class by following the above steps, and remove by entering the index of a course to delete and hitting “remove course”.
![SS3](./images/screenshot.png)
- - - -
#my-readme